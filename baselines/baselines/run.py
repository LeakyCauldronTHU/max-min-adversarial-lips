import sys
import re
import multiprocessing
import os.path as osp
import gym
from collections import defaultdict
import tensorflow as tf
import numpy as np

from baselines.common.vec_env import VecFrameStack, VecNormalize, VecEnv
from baselines.common.vec_env.vec_video_recorder import VecVideoRecorder
from baselines.common.cmd_util import common_arg_parser, parse_unknown_args, make_vec_env, make_env, attack_arg_parser
from baselines.common.tf_util import get_session
from baselines import logger
from importlib import import_module
from baselines.common import set_global_seeds

import os
import subprocess
import datetime
import matplotlib.pyplot as plt

try:
    from mpi4py import MPI
except ImportError:
    MPI = None

try:
    import pybullet_envs
except ImportError:
    pybullet_envs = None

try:
    import roboschool
except ImportError:
    roboschool = None

_game_envs = defaultdict(set)
for env in gym.envs.registry.all():
    # TODO: solve this with regexes
    env_type = env.entry_point.split(':')[0].split('.')[-1]
    _game_envs[env_type].add(env.id)

# reading benchmark names directly from retro requires
# importing retro here, and for some reason that crashes tensorflow
# in ubuntu
_game_envs['retro'] = {
    'BubbleBobble-Nes',
    'SuperMarioBros-Nes',
    'TwinBee3PokoPokoDaimaou-Nes',
    'SpaceHarrier-Nes',
    'SonicTheHedgehog-Genesis',
    'Vectorman-Genesis',
    'FinalFight-Snes',
    'SpaceInvaders-Snes',
}


def train(args, attack_args, extra_args):
    env_type, env_id = get_env_type(args)
    print('env_type: {}'.format(env_type))

    total_timesteps = int(args.num_timesteps)
    seed = args.seed

    learn = get_learn_function(args.alg)
    alg_kwargs = get_learn_function_defaults(args.alg, env_type)

    alg_kwargs['ent_coef'] = args.ent_coef
    alg_kwargs.update(extra_args)
    lr_for_attack_tmp = np.copy(attack_args.lr_for_attack)
    attack_args.lr_for_attack = lambda f: f * lr_for_attack_tmp

    env = build_env(args)
    eval_env = build_env(args)
    eval_env_attack = build_env(args)
    eval_env_other_attack = build_env(args)

    if args.save_video_interval != 0:
        env = VecVideoRecorder(env, osp.join(logger.get_dir(), "videos"), record_video_trigger=lambda x: x % args.save_video_interval == 0, video_length=args.save_video_length)

    if args.network:
        alg_kwargs['network'] = args.network
    else:
        if alg_kwargs.get('network') is None:
            alg_kwargs['network'] = get_default_network(env_type)

    print('Training {} on {}:{} with arguments \n{}'.format(args.alg, env_type, env_id, alg_kwargs))
    print("alg_kwrgs", alg_kwargs)

    model = learn(
        env=env,
        eval_env=eval_env,
        eval_env_attack=eval_env_attack,
        eval_env_other_attack=eval_env_other_attack,
        seed=seed,
        total_timesteps=total_timesteps,
        attack_args=attack_args,
        **alg_kwargs
    )

    return model, env


def build_env(args, seed=None, env_id=None, max_episode_steps=None):
    print("building a new environment")
    ncpu = multiprocessing.cpu_count()
    if sys.platform == 'darwin': ncpu //= 2

    if seed:
        nenv = 1
        alg = 'ppo2'
        seed = seed
        gamestate = None
        reward_scale = 1.0
        env_type = 'atari'
        env_id = env_id
    else:
        nenv = args.num_env or ncpu
        alg = args.alg
        seed = args.seed
        gamestate = args.gamestate
        reward_scale = args.reward_scale
        env_type, env_id = get_env_type(args)

    # print("env type and ID", env_type, env_id)

    if env_type in {'atari', 'retro'}:
        if alg == 'deepq':
            env = make_env(env_id, env_type, seed=seed, wrapper_kwargs={'frame_stack': True})
        elif alg == 'trpo_mpi':
            env = make_env(env_id, env_type, seed=seed)
        else:
            frame_stack_size = 4
            env = make_vec_env(env_id, env_type, nenv, seed, gamestate=gamestate, reward_scale=reward_scale,
                               max_episode_steps=max_episode_steps)
            env = VecFrameStack(env, frame_stack_size)

    else:
        config = tf.ConfigProto(allow_soft_placement=True, intra_op_parallelism_threads=1,
                                inter_op_parallelism_threads=1)

        config.gpu_options.allow_growth = True
        get_session(config=config)

        flatten_dict_observations = alg not in {'her'}
        env = make_vec_env(env_id, env_type, args.num_env or 1, seed,
                           reward_scale=reward_scale, flatten_dict_observations=flatten_dict_observations)

        if env_type == 'mujoco':
            env = VecNormalize(env, use_tf=True)

    return env


def get_env_type(args):
    env_id = args.env

    if args.env_type is not None:
        return args.env_type, env_id

    # Re-parse the gym registry, since we could have new envs since last time.
    for env in gym.envs.registry.all():
        env_type = env.entry_point.split(':')[0].split('.')[-1]
        _game_envs[env_type].add(env.id)  # This is a set so add is idempotent

    if env_id in _game_envs.keys():
        env_type = env_id
        env_id = [g for g in _game_envs[env_type]][0]
    else:
        env_type = None
        for g, e in _game_envs.items():
            if env_id in e:
                env_type = g
                break
        if ':' in env_id:
            env_type = re.sub(r':.*', '', env_id)
        assert env_type is not None, 'env_id {} is not recognized in env types'.format(env_id, _game_envs.keys())

    return env_type, env_id


def get_default_network(env_type):
    if env_type in {'atari', 'retro'}:
        return 'cnn'
    else:
        return 'mlp'


def get_alg_module(alg, submodule=None):
    submodule = submodule or alg
    try:
        # first try to import the alg module from baselines
        alg_module = import_module('.'.join(['baselines', alg, submodule]))
    except ImportError:
        # then from rl_algs
        alg_module = import_module('.'.join(['rl_' + 'algs', alg, submodule]))

    return alg_module


def get_learn_function(alg):
    return get_alg_module(alg).learn


def get_learn_function_defaults(alg, env_type):
    try:
        alg_defaults = get_alg_module(alg, 'defaults')
        kwargs = getattr(alg_defaults, env_type)()
    except (ImportError, AttributeError):
        kwargs = {}
    return kwargs


def parse_cmdline_kwargs(args):
    '''
    convert a list of '='-spaced command-line arguments to a dictionary, evaluating python objects when possible
    '''
    def parse(v):

        assert isinstance(v, str)
        try:
            return eval(v)
        except (NameError, SyntaxError):
            return v

    return {k: parse(v) for k,v in parse_unknown_args(args).items()}


def str_process(strings):
    components = strings.split('_')
    short = ''
    for i in range(len(components)):
        short += components[i][0:3]
        short += '_'
    short = short[:-1]
    return short


def env_process(env):
    if isinstance(env, str):
        if 'NoFrameskip' in env:
            env = env.split('NoFrameskip')[0]

    return env


def delete_old_models(dir, keep=10):
    existing_models = os.listdir(dir)
    num_models = len(existing_models)
    to_deleted = []
    if num_models > keep:
        to_deleted = existing_models[0:num_models-keep]

    for f in to_deleted:
        print("too many models have been found", f)
        cmd = 'rm -rf ' + os.path.join(dir, f)
        print('delete command is', cmd)
        subprocess.call(cmd, shell=True)


def delete_old_logs(dir, logs):
    load_path = ''
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
    try:
        existing_logs = os.listdir(dir)
        print("searching old logs")
        for lg in existing_logs:
            print("list", lg)
            if logs in lg:
                print("we have found old logs:", lg, "which is the same as:", logs)
                # cmd = 'rm -rf ' + os.path.join(dir, lg)
                load_path = os.path.join(dir, lg)
                # print('delete command is', cmd)
                # subprocess.call(cmd, shell=True)
            else:
                pass

    except:
        print("the log directory does not exist", dir)
        pass
    print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")

    return load_path


def get_load_path(dir):
    load_file = None
    files = os.listdir(dir)
    if 'checkpoints' in files:
        logs = os.listdir(osp.join(dir, 'checkpoints'))
        last_in = False
        for lg in logs:
            if 'last' in lg:
                last_in = True
                print("last logs in path, the code has converged, so we terminate the training process")
                # exit(0)

        if last_in is False and len(logs) >= 1:
            try:
                load_file = (logs[-1]).split('.')[0]
            except:
                pass

        if last_in:
            load_file = 'last'

    if load_file:
        load_file = osp.join(dir, 'checkpoints', load_file)

    return load_file


def find_logs(philly_path, log_name):
    logs = os.listdir(philly_path)

    log_path = ''
    # print("log_name", log_name)
    # print("logs", logs)
    for log in logs:
        # print("log", log)
        if log_name in log:
            print("found normally trained models:", log, "which is the same as:", log_name)
            model = (os.listdir(os.path.join(philly_path, log, 'checkpoints')))
            print("models", model)
            if len(model) >= 4:
                model = model[-1]
                log_path = os.path.join(philly_path, log, 'checkpoints', model)
            break
    if log_path:
        return log_path
    else:
        print("normally trained models not found!")
        return None


def main(args):
    # processing args ##################################################################################################
    args_sys = args
    arg_parser = common_arg_parser()
    args, unknown_args = arg_parser.parse_known_args(args_sys)
    extra_args_1 = parse_cmdline_kwargs(unknown_args)

    if args.cluster is not None:
        args.num_env = 16
        args.log_path = '//philly/' + args.cluster + '/resrchvc/v-wancha/max_min_new_logs/'

    attack_args_parser = attack_arg_parser()
    attack_args, unknown_args = attack_args_parser.parse_known_args(args_sys)
    extra_args_2 = parse_cmdline_kwargs(unknown_args)

    attack_args.lr_for_attack = attack_args.lr_for_attack * attack_args.lr_times

    extra_args = dict.fromkeys([x for x in extra_args_1 if x in extra_args_2])
    for t in extra_args.keys():
        extra_args[t] = extra_args_1[t]

    if args.play:
        args.num_timesteps = 0
        args.num_env = 1
        args.save_video_interval = 1
        args.save_video_length = 10000
        attack_args.training_type = 'both'

    args_dict, attack_args_dict = args.__dict__, attack_args.__dict__
    infos_args = ['env', 'seed']
    if attack_args.training_type == 'both':
        infos_attack_args = ['training_type', 'attack_interval', 'stack_attack_network', 'lipschitz_distance',
                             'coef_lips']
        if attack_args.dynamic_clip:
            infos_attack_args.append('dynamic_clip')

    if attack_args.training_type == 'attack':
        infos_attack_args = ['training_type', 'stack_attack_network']

    if attack_args.training_type == 'normal':
        infos_attack_args = ['training_type']

    # infos_extra_args = ['network_size']
    infos_extra_args = []

    extra_args_log = {}
    for i in range(len(infos_extra_args)):
        if infos_extra_args[i] not in extra_args.keys():
            extra_args_log.update({infos_extra_args[i]: None})
    extra_args_log.update(extra_args)

    dir_tmp = ''
    if args.log_path is None:
        args.log_path = '~'
    if args.log_path[0] == '~':
        args.log_path = osp.expanduser(args.log_path)

    for i in range(len(infos_args)):
        dir_tmp = dir_tmp + str_process(infos_args[i]) + '=' + str(env_process(args_dict[infos_args[i]])) + '-'
    for i in range(len(infos_attack_args)):
        dir_tmp = dir_tmp + str_process(infos_attack_args[i]) + '=' + str(attack_args_dict[infos_attack_args[i]]) + '-'
    for i in range(len(infos_extra_args)):
        dir_tmp = dir_tmp + str_process(infos_extra_args[i]) + '=' + str(extra_args_log[infos_extra_args[i]]) + '-'

    log_path = delete_old_logs(args.log_path, dir_tmp)
    if log_path:
        args.log_path = log_path
    else:
        dir_tmp = dir_tmp + datetime.datetime.now().strftime("%m_%d_%H_%M_%S_%f")
        args.log_path = osp.join(args.log_path, dir_tmp)

    if attack_args.training_type == 'attack' and not args.play and args.cluster:
        load_path = r'//philly/' + args.cluster + r'/resrchvc/v-wancha/max_min_logs'
        dir_tmp_tmp = dir_tmp.replace('tra_typ=attack', 'tra_typ=normal')
        dir_tmp_tmp = dir_tmp_tmp.replace('use_att=1.0', 'use_att=0.0')
        dir_tmp_tmp = dir_tmp_tmp.replace('-sta_att_net=' + str(attack_args.stack_attack_network), '')

        print("find logs", dir_tmp_tmp)
        load_path = find_logs(load_path, dir_tmp_tmp)
        extra_args.update({'load_path': load_path})

    if attack_args.training_type == 'further' and not args.play and args.cluster:
        load_path = r'//philly/' + args.cluster + r'/resrchvc/v-wancha/max_min_logs'
        dir_tmp_tmp = dir_tmp.replace('tra_typ=further', 'tra_typ=both')
        dir_tmp_tmp = dir_tmp_tmp.replace('-fur_typ=full', '')
        dir_tmp_tmp = dir_tmp_tmp.replace('-fur_typ=partial', '')

        print("find logs", dir_tmp_tmp)
        load_path = find_logs(load_path, dir_tmp_tmp)
        extra_args.update({'load_path': load_path})
        if load_path is None:
            print("the model to be load is not well trained")
            exit(0)

    if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
        rank = 0
        logger.configure(dir=args.log_path)
    else:
        logger.configure(format_strs=[])
        rank = MPI.COMM_WORLD.Get_rank()

    load_path = get_load_path(args.log_path)
    if load_path:
        print("the load path is", load_path)
        extra_args.update({'load_path': load_path})
    if load_path:
        print("the load path is not None, load models")
        if 'last' in load_path:
            attack_args.beginner = int(1e10)
        else:
            beginner = load_path[-5:]
            beginner = int(beginner) + 1
            print("loaded checkpoint is:", beginner - 1, "update starts at:", beginner)
            attack_args.beginner = beginner
    else:
        print("no checkpoints found, the model will be trained from stratch")

    if load_path:
        logger.log("***********************************************")
        logger.log("*********** the code restarts here ************")
        logger.log("*******************common args************************")
        for k in args_dict.keys():
            logger.log(k, args_dict[k])
        logger.log("*******************attack args************************")
        for k in attack_args_dict.keys():
            logger.log(k, attack_args_dict[k])
        logger.log("*******************extra args*************************")
        for k in extra_args.keys():
            logger.log(k, extra_args[k])
        logger.log("******************************************************")
    else:
        logger.log("*******************common args************************")
        for k in args_dict.keys():
            logger.log(k, args_dict[k])
        logger.log("*******************attack args************************")
        for k in attack_args_dict.keys():
            logger.log(k, attack_args_dict[k])
        logger.log("*******************extra args*************************")
        for k in extra_args.keys():
            logger.log(k, extra_args[k])
        logger.log("******************************************************")
    # finish logs processing ###########################################################################################

    model, env = train(args, attack_args, extra_args)

    # saving the final trained model ###################################################################################
    for i in range(10):
        try:
            save_path = osp.join(args.log_path, 'checkpoints', 'last')
            if rank == 0:
                model.save(save_path)
            break
        except:
            print("failed to save the last model, retry ......")
            pass
    # finished saving models ###########################################################################################

    ####################################################################################################################
    # begin evaluation #################################################################################################
    logger.log("********************** start evaluating the learned policy *********************************")
    logger.log("******************** first we evaluate the unattacked policy *******************************")

    num_policy_seed = attack_args.num_policy_seed
    num_env_seed = attack_args.num_env_seed
    num_episode = attack_args.num_episode

    seeds = [1000 + i for i in range(num_policy_seed)]
    seeds_env = [2000 + i for i in range(num_env_seed)]

    returns = []
    for i in range(len(seeds)):  # policy seeds
        set_global_seeds(seeds[i])
        episode_return = []
        for j in range(len(seeds_env)):  # environment seeds
            print("evaluating env seed {} in global seed {}".format(j, i))
            env = build_env(args=None, seed=seeds_env[j], env_id=attack_args.env, max_episode_steps=10000)
            obs = env.reset()
            episode_rew = 0

            episode_counter = 0
            while True:
                # print("step counter", step_counter)
                # actions, _, _, _ = model.step_eval(obs)
                print("check obs", np.shape(obs))
                exit(0)
                actions, _, _, _ = model.step_attacked(obs)
                # actions, _, _, _ = model.step(obs)

                obs, rew, done, info = env.step(actions)

                # env.render()
                episode_rew += rew
                # print("info", info)
                if done:
                    # episode_counter += 1
                    if info[0].get('episode'):
                        episode_counter += 1
                        print("episode reward is {}".format(info[0]['episode']['r']))
                    episode_return.append(episode_rew)
                    episode_rew = 0
                    obs = env.reset()
                    if episode_counter > num_episode:
                        break

                returns += episode_return
            logger.log("episode return", sum(episode_return) / len(episode_return))
            env.close()
    logger.log("********************** unattacked evaluating results is as below ***************************")
    logger.log("unattacked policy performance:", sum(returns) / len(returns))

    logger.log("********************************************************************************************")
    logger.log("*********************** next we evaluate the attacked policy *******************************")

    returns = []
    for i in range(len(seeds)):  # policy seeds
        set_global_seeds(seeds[i])
        episode_return = []
        for j in range(len(seeds_env)):  # environment seeds
            print("evaluating env seed {} in global seed {}".format(j, i))
            env = build_env(args=None, seed=seeds_env[j], env_id=attack_args.env, max_episode_steps=10000)
            obs = env.reset()
            episode_rew = 0

            episode_counter = 0
            while True:
                # print("step counter", step_counter)
                # actions, _, _, _ = model.step_eval(obs)
                actions, _, _, _ = model.step_attacked(obs)
                obs, rew, done, info = env.step(actions)
                # env.render()
                episode_rew += rew
                # print("info", info)
                if done:
                    # episode_counter += 1
                    if info[0].get('episode'):
                        episode_counter += 1
                        print("episode reward is {}".format(info[0]['episode']['r']))
                    # print('episode_rew={}'.format(episode_rew))
                    episode_return.append(episode_rew)
                    episode_rew = 0
                    obs = env.reset()
                    if episode_counter > num_episode:
                        break

                returns += episode_return
            # logger.log("episode return", sum(episode_return) / len(episode_return))
            env.close()

    ####################################################################################################################
    ####### evaluation #################################################################################################
    if args.play:
        logger.log("Running trained model")
        obs = env.reset()

        episode_rew = 0
        counter = 0
        while True:
            counter += 1
            adversaries = model.get_adversaries(obs)
            other_adversaries = model.get_other_adversaries(obs)
            if args.test_type == 'normal':
                actions, _, _, _ = model.step(obs)
            elif args.test_type == 'attack':
                actions, _, _, _ = model.step_attacked(obs)
            elif args.test_type == 'other_attack':
                actions, _ = model.step_action_with_other_adversaries(obs)

            if args.plot and np.mod(counter, 100) == 0:
                plt.figure()
                plt.subplot(1,3,1)
                plt.imshow(adversaries[0,:,:,0])
                plt.subplot(1,3,2)
                plt.imshow(other_adversaries[0,:,:,0])
                plt.subplot(1,3,3)
                plt.imshow((obs+adversaries)[0,:,:,0])
                plt.show()
                plt.close()

            obs, rew, done, info = env.step(actions)
            # print("info", done, info)
            episode_rew += rew[0] if isinstance(env, VecEnv) else rew
            env.render()
            done = done.any() if isinstance(done, np.ndarray) else done
            if done:
                print('episode_rew={}'.format(episode_rew))
                episode_rew = 0
                obs = env.reset()

    env.close()

    return model


if __name__ == '__main__':
    main(sys.argv)
