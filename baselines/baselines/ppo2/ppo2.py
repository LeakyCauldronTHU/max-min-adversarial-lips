import os
import time
import numpy as np
import os.path as osp
from baselines import logger
from collections import deque
from baselines.common import explained_variance, set_global_seeds
from baselines.common.policies import build_policy
try:
    from mpi4py import MPI
except ImportError:
    MPI = None
from baselines.ppo2.runner import Runner

import copy
from baselines.run import delete_old_models


def constfn(val):
    def f(_):
        return val
    return f


def learn(*, network, env, total_timesteps, eval_env = None, eval_env_attack = None, eval_env_other_attack = None, seed=None, nsteps=2048, ent_coef=0.0, lr=3e-4,
            vf_coef=0.5,  max_grad_norm=0.5, gamma=0.99, lam=0.95,
            log_interval=1, nminibatches=4, noptepochs=4, cliprange=0.2,
            save_interval=1000, load_path=None, load_scope=None, model_fn=None, update_fn=None, init_fn=None, mpi_rank_weight=1,
          comm=None, attack_args=None, **network_kwargs):
    '''
    Learn policy using PPO algorithm (https://arxiv.org/abs/1707.06347)

    Parameters:
    ----------

    network:                          policy network architecture. Either string (mlp, lstm, lnlstm, cnn_lstm, cnn, cnn_small, conv_only - see baselines.common/models.py for full list)
                                      specifying the standard network architecture, or a function that takes tensorflow tensor as input and returns
                                      tuple (output_tensor, extra_feed) where output tensor is the last network layer output, extra_feed is None for feed-forward
                                      neural nets, and extra_feed is a dictionary describing how to feed state into the network for recurrent neural nets.
                                      See common/models.py/lstm for more details on using recurrent nets in policies

    env: baselines.common.vec_env.VecEnv     environment. Needs to be vectorized for parallel environment simulation.
                                      The environments produced by gym.make can be wrapped using baselines.common.vec_env.DummyVecEnv class.


    nsteps: int                       number of steps of the vectorized environment per update (i.e. batch size is nsteps * nenv where
                                      nenv is number of environment copies simulated in parallel)

    total_timesteps: int              number of timesteps (i.e. number of actions taken in the environment)

    ent_coef: float                   policy entropy coefficient in the optimization objective

    lr: float or function             learning rate, constant or a schedule function [0,1] -> R+ where 1 is beginning of the
                                      training and 0 is the end of the training.

    vf_coef: float                    value function loss coefficient in the optimization objective

    max_grad_norm: float or None      gradient norm clipping coefficient

    gamma: float                      discounting factor

    lam: float                        advantage estimation discounting factor (lambda in the paper)

    log_interval: int                 number of timesteps between logging events

    nminibatches: int                 number of training minibatches per update. For recurrent policies,
                                      should be smaller or equal than number of environments run in parallel.

    noptepochs: int                   number of training epochs per update

    cliprange: float or function      clipping range, constant or schedule function [0,1] -> R+ where 1 is beginning of the training
                                      and 0 is the end of the training

    save_interval: int                number of timesteps between saving events

    load_path: str                    path to load the model from

    **network_kwargs:                 keyword arguments to the policy / network builder. See baselines.common/policies.py/build_policy and arguments to a particular type of network
                                      For instance, 'mlp' network architecture has arguments num_hidden and num_layers.



    '''

    set_global_seeds(seed)
    lr_for_attack = attack_args.lr_for_attack

    if isinstance(lr, float): lr = constfn(lr)
    else: assert callable(lr)

    if isinstance(lr_for_attack, float): lr_for_attack = constfn(lr_for_attack)
    else: assert callable(lr_for_attack)

    if isinstance(cliprange, float): cliprange = constfn(cliprange)
    else: assert callable(cliprange)
    total_timesteps = int(total_timesteps)

    policy = build_policy(env, network, attack_args=attack_args, **network_kwargs)

    # Get the nb of env
    nenvs = env.num_envs

    # Get state_space and action_space
    ob_space = env.observation_space
    ac_space = env.action_space

    # Calculate the batch_size
    nbatch = nenvs * nsteps
    nbatch_train = nbatch // nminibatches
    is_mpi_root = (MPI is None or MPI.COMM_WORLD.Get_rank() == 0)

    # Instantiate the model object (that creates act_model and train_model)
    if model_fn is None:
        from baselines.ppo2.model import Model
        model_fn = Model

    model = model_fn(policy=policy, ob_space=ob_space, ac_space=ac_space, nbatch_act=nenvs, nbatch_train=nbatch_train,
                    nsteps=nsteps, ent_coef=ent_coef, vf_coef=vf_coef,
                    max_grad_norm=max_grad_norm, attack_args=attack_args, comm=comm, mpi_rank_weight=mpi_rank_weight)

    if load_path is not None:
        model.load(load_path)
    # Instantiate the runner object
    runner = Runner(env=env, model=model, nsteps=nsteps, gamma=gamma, lam=lam)
    eval_runner, eval_runner_attack, eval_runner_other_attack = None, None, None
    if eval_env is not None:
        eval_runner = Runner(env=eval_env, model=model, nsteps=nsteps, gamma=gamma, lam=lam)
    if eval_env_attack is not None:
        eval_runner_attack = Runner(env=eval_env_other_attack, model=model, nsteps=nsteps, gamma=gamma, lam=lam)
    if eval_env_other_attack is not None:
        eval_runner_other_attack = Runner(env=eval_env_other_attack, model=model, nsteps=nsteps, gamma=gamma, lam=lam)

    epinfobuf = deque(maxlen=100)
    eval_epinfobuf, eval_epinfobuf_attack, eval_epinfobuf_other_attack = None, None, None
    if eval_env is not None:
        eval_epinfobuf = deque(maxlen=100//attack_args.eval_freq)
    if eval_env_attack is not None:
        eval_epinfobuf_attack = deque(maxlen=100//attack_args.eval_freq)
    if eval_env_other_attack is not None:
        eval_epinfobuf_other_attack = deque(maxlen=100//attack_args.eval_freq)

    if init_fn is not None:
        init_fn()

    # Start total timer
    tfirststart = time.perf_counter()
    nupdates = total_timesteps//nbatch

    policy_clipfrac, policy_approxkl = None, None
    clip_shrink = copy.deepcopy(attack_args.clip_shrink)

    flip = False
    for update in range(attack_args.beginner, nupdates+1):
        if update % attack_args.alter_freq == 0:
            flip = not flip
            if flip:
                print("****************************************************************")
                print("***** the following {} updates focus on policy training *******".format(attack_args.alter_freq))
                print("****************************************************************")
            else:
                print("****************************************************************")
                print("**** the following {} updates focus on adversarial training ***".format(attack_args.alter_freq))
                print("****************************************************************")

        assert nbatch % nminibatches == 0
        # Start timer
        tstart = time.perf_counter()
        frac = 1.0 - (update - 1.0) / nupdates
        # Calculate the learning rate
        lrnow = lr(frac) * attack_args.lr_shrink

        lr_for_attack_now = lr_for_attack(frac)
        # Calculate the cliprange
        cliprangenow = cliprange(frac)
        cliprangenow_shrink = None

        if update % log_interval == 0 and is_mpi_root: logger.info('Stepping environment...')

        # Get minibatch
        start = time.time()
        obs, returns, masks, actions, values, neglogpacs, states, epinfos = runner.run(run_type='normal')

        end = time.time()
        epinfobuf.extend(epinfos)
        print("time used for normal rollouts", end - start)
        start = time.time()
        # eval_epinfos, eval_epinfos_attack, eval_epinfos_other_attack = None, None, None
        if attack_args.training_type == 'both':
            if eval_env is not None and np.mod(update, attack_args.eval_freq) == 0:
                eval_epinfos = eval_runner.run(run_type='eval_free')
                eval_epinfobuf.extend(eval_epinfos)
            if eval_env_attack is not None and np.mod(update, attack_args.eval_freq) == 0:
                eval_epinfos_attack = eval_runner_attack.run(run_type='eval_attack')
                eval_epinfobuf_attack.extend(eval_epinfos_attack)
            if eval_env_other_attack is not None and np.mod(update, attack_args.eval_freq) == 0:
                eval_epinfos_other_attack = eval_runner_other_attack.run(run_type='eval_other_attack')
                eval_epinfobuf_other_attack.extend(eval_epinfos_other_attack)

        elif attack_args.training_type == 'normal':
            if eval_env is not None and np.mod(update, attack_args.eval_freq) == 0:
                eval_epinfos = eval_runner.run(run_type='eval_free')

        else:
            print("illegal training type")
            exit(0)
        end = time.time()
        print("time used for evaluation rollouts", end - start)

        if update % log_interval == 0 and is_mpi_root: logger.info('Done.')

        # Here what we're going to do is for each minibatch calculate the loss and append it.
        mblossvals = []
        if states is None: # nonrecurrent version
            # Index of each element of batch_size
            # Create the indices array
            inds = np.arange(nbatch)
            start_time = time.time()
            for _ in range(noptepochs):
                # Randomize the indexes
                np.random.shuffle(inds)
                # 0 to batch_size with batch_train_size step
                mblossvals_dynamic = []
                cliprangenow_shrink = cliprange(frac) * clip_shrink
                for start in range(0, nbatch, nbatch_train):
                    end = start + nbatch_train
                    mbinds = inds[start:end]
                    slices = (arr[mbinds] for arr in (obs, returns, masks, actions, values, neglogpacs))
                    if attack_args.training_type == 'both':
                        returned = model.train(update, flip, lr_for_attack_now, lrnow, cliprangenow_shrink,
                                    cliprangenow, *slices)
                        mblossvals.append(returned[0])
                        # print(returned[0], "returned1", returned[1])
                        if returned[1][0] is not None:
                            mblossvals_dynamic.append(returned[1])
                    else:
                        mblossvals.append(model.train(update, flip, lr_for_attack_now, lrnow, cliprangenow_shrink,
                                                      cliprangenow, *slices))

                # dynamically adjust the clip range coefficient
                if mblossvals_dynamic:
                    lossvals_dynamic = np.mean(mblossvals_dynamic, axis=0)
                    policy_approxkl, policy_clipfrac = lossvals_dynamic[0], lossvals_dynamic[1]

                    if attack_args.dynamic_clip > 0:
                        if policy_approxkl > attack_args.target_kl * 1.2:
                            clip_shrink /= 2
                            clip_shrink = max(clip_shrink, attack_args.min_shrink)
                        elif policy_approxkl < attack_args.target_kl / 2.0:
                            clip_shrink *= 1.2
                            clip_shrink = min(clip_shrink, attack_args.clip_shrink)

                        print("update={}, clip_shrink={}".format(update, clip_shrink))

            end_time = time.time()
            print("time elapsed for training is", end_time - start_time)
        else: # recurrent version
            pass

        # Feedforward --> get losses --> update
        lossvals = np.mean(mblossvals, axis=0)

        # End timer
        tnow = time.perf_counter()
        # Calculate the fps (frame per second)
        fps = int(nbatch / (tnow - tstart))
        if update_fn is not None:
            update_fn(update)

        if update % log_interval == 0 or update == 1:
            # Calculates if value function is a good predicator of the returns (ev > 1)
            # or if it's just worse than predicting nothing (ev =< 0)
            ev = explained_variance(values, returns)
            logger.logkv("misc/lr", lrnow)
            logger.logkv('misc/clip', cliprangenow)
            logger.logkv("misc/lr_for_attack", lr_for_attack_now)
            logger.logkv('misc/clip_shrink', cliprangenow_shrink)
            logger.logkv("misc/serial_timesteps", update*nsteps)
            logger.logkv("misc/nupdates", update)
            logger.logkv("misc/total_timesteps", update*nbatch)
            logger.logkv("fps", fps)
            logger.logkv("misc/explained_variance", float(ev))
            logger.logkv('eprewmean', safemean([epinfo['r'] for epinfo in epinfobuf]))
            logger.logkv('eplenmean', safemean([epinfo['l'] for epinfo in epinfobuf]))

            if eval_env is not None and np.mod(update, attack_args.eval_freq) == 0:
                logger.logkv('eval_eprewmean', safemean([epinfo['r'] for epinfo in eval_epinfobuf]))
                logger.logkv('eval_eplenmean', safemean([epinfo['l'] for epinfo in eval_epinfobuf]))

            if attack_args.training_type == 'both':
                if eval_env_attack is not None and np.mod(update, attack_args.eval_freq) == 0:
                    logger.logkv('eval_eprewmean_attack', safemean([epinfo['r'] for epinfo in eval_epinfobuf_attack]) )
                    logger.logkv('eval_eplenmean_attack', safemean([epinfo['l'] for epinfo in eval_epinfobuf_attack]) )
                if eval_env_other_attack is not None and np.mod(update, attack_args.eval_freq) == 0:
                    logger.logkv('eval_eprewmean_other_attack', safemean([epinfo['r'] for epinfo in eval_epinfobuf_other_attack]) )
                    logger.logkv('eval_eplenmean_other_attack', safemean([epinfo['l'] for epinfo in eval_epinfobuf_other_attack]) )

            logger.logkv('misc/time_elapsed', tnow - tfirststart)
            for (lossval, lossname) in zip(lossvals, model.loss_names):
                logger.logkv('loss/' + lossname, lossval)

            logger.dumpkvs()

        for i in range(10):
            try:
                if save_interval and (update % save_interval == 0 or update == 1) and logger.get_dir() and (
                        MPI is None or MPI.COMM_WORLD.Get_rank() == 0):
                    checkdir = osp.join(logger.get_dir(), 'checkpoints')
                    # first delete very old models
                    delete_old_models(checkdir)

                    # then store new models
                    os.makedirs(checkdir, exist_ok=True)
                    savepath = osp.join(checkdir, '%.5i' % update)
                    print('Saving to', savepath)
                    model.save(savepath)
                    break
            except:
                print("failed to save the model, retry")
                time.sleep(5)
                pass

    return model
# Avoid division error when calculate the mean (in our case if epinfo is empty returns np.nan, not return an error)
def safemean(xs):
    return np.nan if len(xs) == 0 else np.mean(xs)



