# coding=utf-8
import os
import io
import subprocess
import json
import csv

from datetime import datetime

code_name = 'adversary-support-alternate'

cluster = "gcr"

# cluster = "rr2"

# cluster = 'rr1'

# cluster = "cam"

version = "start"

PHILLY_FS = r'"\\scratch2\scratch\Philly\philly-fs\windows\philly-fs.exe"'
# PHILLY_FS = r'"D:\philly-fs.exe"'

vc = "resrchvc"
user = 'v-wancha'
passwd = ''
# proj = '<Your code path under /philly/eu1/pnrsy/your_alias/>'
os.environ["PHILLY_VC"] = vc


def philly_cmd(commandline):
    print("philly_FS", PHILLY_FS)
    print("philly cmd", commandline)
    subprocess.call(r'{0} {1}'.format(PHILLY_FS, commandline), shell=True)


def abort_job(jobId, submit=False):
    head_url = "https://philly/api/abort?"
    CMD = "clusterId={0}&".format(cluster)
    CMD += "jobId={0}&".format(jobId)
    url = head_url + CMD

    print("~~~~~~~~~~~~~~~~~~~~~abort job~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("url", url)
    response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(url)).readlines()
    print(response)

    print("\n~~~~~~~~~~~~~~~~~~finish abortion~~~~~~~~~~~~~~~~~~~~~")


def get_appID(state):
    response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(
        'https://philly/api/list?clusterId=' + cluster + '&vcId=resrchvc&userName=v-wancha&numFinishedJobs=5000')).readlines()
    print(type(response), len(response))
    content = json.loads(response[0])
    # print(content)
    appIDs = []

    for i in range(len(content[state])):
        name = content[state][i]['name']
        if 'env' in name:
            name = name.split('!')[0]
            name = name.split('ns-p-')[1]
            # print(name)
            appIDs.append([content[state][i]['appID'], name])

    return appIDs


if __name__ == "__main__":
    jobIds = get_appID('runningJobs')
    # print(jobIds)
    for app in jobIds:
        print("app", app[0])
        try:
            abort_job(app[0])
            # exit(0)
        except:
            print("failed to abort jobs")
            pass
